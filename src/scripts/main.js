(function($) {
  $('.aoa-accordion .aoa-accordion__items section').each(function(i, elem) {
    var self = $(this);
    var waypoint = new Waypoint({
      element: self[0],
      handler: function(direction) {
        if (direction === 'down') {
          self.addClass('on');
        }
      },
      offset: '94%',
    });
  });
})(jQuery)

