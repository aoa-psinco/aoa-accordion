<?php 
/*
Plugin Name: AOA Accordion CBS
Description: Shortcodes to create an accordion
Version:  0.1.0
Author: Patrick Sinco
*/

if ( ! defined( 'WPINC' ) ) {
  die;
}

function aoa_accordion_shortcode_init () {
  if ( ! shortcode_exists( 'accordion' ) ) {

    /**
     * Create the shortcode.
     *
     * @param array $atts     The shortcode attributes
     * @param array $content  The shortcode content
     * @return string
     */
    function aoa_accordion_shortcode( $atts, $content = null ) {

      wp_enqueue_style( 'aoa_accordion_styles' );
      wp_enqueue_script( 'aoa_accordion_scripts' );

      $shortcode_atts = shortcode_atts( array( 'id' => '' ), $atts );

      $id = $atts['id'];
      $container = '<div class="aoa-accordion"' . ( ! empty( $id ) ? " id=\"$id\"" : '' ) . '>';

      $markup = sprintf( '%s%s%s%s',
                         $container,
                         '<div class="aoa-accordion__items">',
                         $content,
                         '</div></div>' );

      return do_shortcode( $markup );
    }
  }
  add_shortcode( 'accordion', 'aoa_accordion_shortcode' );

  if ( ! shortcode_exists( 'accordion-section' ) ) {
    /**
     * Create the shortcode.
     *
     * @param array $atts     The shortcode attributes
     * @param string $content The shortcode content
     * @return string
     */
    function aoa_accordion_section_shortcode( $atts, $content = null ) 
    {
      $shortcode_atts = shortcode_atts(
        array(
          'title' => '',
          'class' => 'accordion-head',
        ),
        $atts
      );

      $markup  = '<section>';
      $markup .= '  <input type="checkbox" checked="checked">';
      $markup .= '  <i class="aoa-accordion__icon"></i>';
      $markup .= '  <h2 class="' . $shortcode_atts['class'] . '">' . $shortcode_atts['title'] . '</h2>' . $content . '</section>';

      return do_shortcode( $markup );
    }
  }
  add_shortcode( 'accordion-section', 'aoa_accordion_section_shortcode' );

  if ( ! shortcode_exists( 'accordion-item' ) ) {
    /**
     * Create the shortcode.
     *
     * @param array $atts     The shortcode attributes
     * @param string $content The shortcode content
     * @return string
     */
    function aoa_accordion_item_shortcode( $atts, $content = null ) 
    {
      $shortcode_atts = shortcode_atts(
        array(
          'side-padding' => 'false',
          'top-padding' => 'true',
        ),
        $atts
      );

      $shortcode_atts = aoa_accordion_format_atts( $shortcode_atts );

      $side_padding = $shortcode_atts['side_padding'] === 'false' ? 'padding-left: 0; padding-right: 0;' : '';
      $top_padding = $shortcode_atts['top_padding'] === 'true' ? '' : 'padding-top: 0;';
      $padding = (bool) $shortcode_atts['side_padding'] || 
                 (bool) $shortcode_atts['top_padding'] ? 
                    sprintf('style="%s %s"', $side_padding, $top_padding ) : '';

      $markup = sprintf( '%s%s%s', 
                         '<div class="aoa-accordion__item"' . $padding . '><div class="aoa-accordion__body">',
                         $content,
                         '</div></div>' );

      return do_shortcode( $markup );
    }
  }
  add_shortcode( 'accordion-item', 'aoa_accordion_item_shortcode' );
}

/**
 * Change a hyphen to an underscore in the keys to an array.
 *
 * @param array $atts The shortcode attributes
 * @return array $atts The shortcode attributes with replaced keys
 */
function aoa_accordion_format_atts( $atts ) {
  return array_combine(
    array_map( function( $key ) use ( $atts ) { 
      return str_replace( '-', '_', $key );
    }, array_keys( $atts ) ), 
    array_values( $atts )
  );
}
add_action( 'init' , 'aoa_accordion_shortcode_init' );

/**
 * @see https://plugins.trac.wordpress.org/browser/
 *      shortcode-empty-paragraph-fix/trunk/shortcode-empty-paragraph-fix.php
 */
function aoa_accordion_empty_graf_fix( $content ) {
  if (! has_shortcode( $content, 'accordion' ) ) {
    return $content;
  }

  $array = array(
    '<p>[' => '[',
    ']</p>' => ']',
    ']<br />' => ']',
  );

  return strtr( $content, $array );
}
add_filter( 'the_content', 'aoa_accordion_empty_graf_fix' );

function aoa_accordion_register_styles() {
  $css_file = 'aoa-accordion.min.css';
  $css_path = "public/styles/$css_file";
  $js_file = 'aoa-accordion-bundle.js';
  $js_path = "public/scripts/$js_file";

  wp_register_style(
    'aoa_accordion_styles',
    plugins_url( $css_path, __FILE__ ),
    array(),
    filemtime( plugin_dir_path(__FILE__) . "/" . $css_path ),
    'all'
  );

  wp_register_script(
    'aoa_accordion_scripts',
    plugins_url( $js_path, __FILE__ ),
    array('jquery', 'sage/js'),
    filemtime( plugin_dir_path(__FILE__) . "/" . $js_path ),
    true
  );
}
add_action( 'wp_enqueue_scripts' , 'aoa_accordion_register_styles' );
